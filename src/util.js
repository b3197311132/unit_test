function div_check(n) {
    if (typeof n !== "number") {
        return undefined;
    } else if (n % 5 === 0 || n % 7 === 0) {
        return true;
    } else {
        return false;
    }
}

function factorial(n){
	if(typeof n !== "number") return undefined;
	if(n < 0) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;
	return n * factorial(n-1);
};


module.exports = {
    factorial: factorial,
    div_check: div_check
};